# processed_data_daily

Updated: 24 February 2021. 

## Summary

Climate data for _Silene acaulis_ demography sites in Sweden from Automatic Weather Station (AWS) data. Temperature data adjusted with moist adiabatic lapse rate and no adjustments made to the precipitation data. Data calculated by N. Chardon (nathalie.chardon[at]gmail.com). 

All files and data stored in Gitlab repository (https://gitlab.com/nathaliechardon/silene_aws).

## Sites

### Sweden (SE)

- **SA1**: Abisko (1114 m), sampling years 2011-2020
- **SA2**: Abisko (1042 m), sampling years 2012-2020
- **SJ1**: Jämtland (1137 m), sampling years 2011-2020
- **SJ2**: Jämtland (1182 m), sampling years 2013-2020

## AWS Information

Please see README in main repository for information on AWS and calculations. For these daily data, the downloaded files were processed with a) temperature adjustment with moist adiabatic lapse rate and b) retention of data for years 3-5 years prior to first year of sampling. 

## Data structure

The AWS daily data for each population (total n = 4) are provided in both CSV and RData files in 'processed_data_daily'. CSV files are named location_population_elevation_years, and RData files are named by population only. Note that unlike in the monthly means, these data have daily climate for years prior to first sampling year and this is reflected in the naming convention. Rows correspond to days. Columns correspond to:

- year - year of data collection 
- month - month of data collection
- day - day of data collection
- precip - daily cumulative precipitation (mm)
- temp - daily mean temperature (ºC)