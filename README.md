# silene_aws

Updated: 30 March 2021. 

## Summary

Climate data for _Silene acaulis_ demography sites in Europe from Automatic Weather Station (AWS) data. Temperature data adjusted with moist adiabatic lapse rate and no adjustments made to the precipitation data. Data calculated by N. Chardon (nathalie.chardon[at]gmail.com). 

All files and data stored in Gitlab repository (https://gitlab.com/nathaliechardon/silene_aws) and DukeBox.

## Sites

### Switzerland (CH)

- **SV1**: Val Bercla (2488 m), sampling years 2011-2020
- **SV2**: Val Bercla (2568 m), sampling years 2013-2020

### Sweden (SE)

- **SA1**: Abisko (1114 m), sampling years 2011-2020
- **SA2**: Abisko (1042 m), sampling years 2012-2020
- **SJ1**: Jämtland (1137 m), sampling years 2011-2020
- **SJ2**: Jämtland (1182 m), sampling years 2013-2020

### Slovakia (SK)

- **SS1**: Tatra Mountains (1945 m), sampling years 2013-2020
- **SS2**: Tatra Mountains (1927 m), sampling years 2014-2020

### Spain (SP)

- **SP2**: Pyrenees (1987 m), sampling years 2011-2018
- **SP1**: Pyrenees (2663 m), sampling years 2012-2017

## Choice of AWS

We identified the geographically closest AWS to each population with both precipitation and temperature data available to extract climate data. In the case of the Abisko SA2 population, the closest AWS (Abisko) lacked temperature data in the last year of sampling, so we used the next-closest AWS (Abisko Aut) to obtain these. Metadata for each AWS is found in the file 'metadata.xlsx'.

## Obtaining AWS data

AWS data was either directly downloaded from openly accessible sources, or obtained by project collaborators and stored in 'raw_data'. Original data sources and details are listed in 'metadata.xlsx'. These data were then stripped to date and temp/precip values only, and stored in 'clean_data'. Please see the DukeBox or contact N. Chardon for these folders, as the data they contain cannot be publically stored on GitLab. 

## Calculations

We calculated cumulative precipitation, mean temperature, maximum temperature, minimum temperature, and growing degree days (number of days where mean daily temperature > 5ºC) on a monthly scale for the years of demography surveys per site. We adjusted temperature values from AWS data with the standard moist adiabatic lapse rate (0.0065 ºC/m). These calculations are in 'aws.R'.

## Data structure

The AWS data for each population (n = 2/site, total n = 10) are provided in both CSV and RData files in 'processed_data'. CSV files are named location_population_elevation_years, and RData files are named by population only. Rows correspond to months within survey years. Columns correspond to:

- year - year of data collection 
- month - month of data collection
- precip - monthly cumulative precipitation (mm)
- temp - monthly mean temperature (ºC)
- tempmx - maximum daily mean temperature of the month (ºC)
- tempmn - minimum daily mean temperature of the month (ºC)
- gdd - growing degree days of the month (mean daily temperature > 5ºC)

Note that all temperature variables (temp, tempmx, tempmn, gdd) are all highly correlated (Pearson's correlation coefficient > 0.7), and thus caution should be exerted when using more than one temperature variable in multivariate analyses. 

## Caveats

AWS data can be a poor representation of the climate experienced by low-growing plants, as they measure temperature and precipitation at 2 m and 1.5 m above the ground, respectively. Furthermore, AWSs often underestimate precipitation due to measurement inaccuracy and wind, which influences how much precipitation lands inside the gauge. These values also do not account for blowing snow, which is an important factor influencing snow distribution, and therefore Spring melt out and soil moisture, in alpine and arctic regions. Global climate data at 1 km (e.g., CHELSA) likely provide a better estimation of the climate at each population as these data are interpolated with more sophisticated methods, however the full sampling time frame is not available. CHELSA data might be available through 2020 soon, as stated by the data curator Dirk Karger in mid-2020.
