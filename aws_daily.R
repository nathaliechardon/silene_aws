## N. Chardon
## Start: 24 Feb 2020
## Last updated: 24 Feb 2021
## Aims: 
## 1. Process Automatic Weather Station (AWS) data for SE Demography Sites at daily resolution
## 2. Create new DF with climatic data for each population (n = 10)
## 3. Adjust temperature with Moist Adiabatic Lapse Rate (MALR) = 0.0065ºC/m


rm(list=ls())


## LIBRARIES ##
library(stringr)


## WORKING DIRECTORIES ##

# Repository 'silene_aws'
# Note to users: change path to reflect working directory where repository is stored locally
repo <- '~/Desktop/Research/val_bercla/silene_euro_demo/silene_aws/'

# Climate data input
raw.dat <- paste(repo, 'clean_data/', sep = '')
malr <- 0.0065 #moist adiabatic lapse rate 

# # Climate dat output
proc.dat <- paste(repo, 'processed_data_daily/', sep = '')




####################################################################################################

# # Abisko (SE): SA1, 1114 m # # 

# AWS: Katterjakk A, 514 m #

####################################################################################################

# Load data
setwd(raw.dat)
tt <- read.csv('SE/temp_katterjakk-a.csv', stringsAsFactors = F) 
pp <- read.csv('SE/precip_katterjakk-a.csv', stringsAsFactors = F)


# Split date into year, month, day columns
date <- str_split_fixed(tt$date, "/", 3)
tt$year <- as.numeric(date[,3])
tt$month <- as.numeric(date[,1])
tt$day <- as.numeric(date[,2])
tt$date <- NULL

date <- str_split_fixed(pp$date, "/", 3)
pp$year <- as.numeric(date[,3])
pp$month <- as.numeric(date[,1])
pp$day <- as.numeric(date[,2])
pp$date <- NULL

# Adjust temp with MALR
delta <- (514 - 1114) * malr #(elev AWS - elev pop) * malr
tt$temp <- tt$temp + delta

# Combine temp and precip data
tp <- data.frame(year = tt$year, month = tt$month, day = tt$day, 
                 precip = pp$precip, temp = tt$temp) 

# Standardize date columns 
for (i in 1:nrow(tp)) {
  if (nchar(tp$year[i]) == 1) {
    tp$year[i] <- paste('200', tp$year[i], sep = '') #adjust single digit years
  }
  if (nchar(tp$year[i]) == 2) {
    tp$year[i] <- paste('20', tp$year[i], sep = '') #adjust double digit years
  }
}

tp$year <- as.numeric(tp$year)


# # CLIMATE AT SA1, 2011-2020 # #

# Keep only climate corresponding to sampling years + 2 years prior (NOTE: no data prior to Nov. 2008)
yy <- c('2008|2009|2010|2011|2012|2013|2014|2015|2016|2017|2018|2019|2020')
sa1 <- tp[grep(yy, tp$year),]


# Check data 
str(sa1) #data format

summary(sa1) #data summary

par(mfrow = c(2,3))
for (i in 4:ncol(sa1)) { #data distribution
  hist(sa1[,i], breaks = 100)
  boxplot(sa1[,i], horizontal = T)
  dotchart(sa1[,i], labels = '')
}

foo <- sa1[complete.cases(sa1[3:ncol(sa1)]),]
cor(foo[4:ncol(foo)]) #correlations


# Save DF
setwd(proc.dat)
save(sa1, file = 'daily_sa1.RData')
write.csv(sa1, file = 'daily_abisko_sa1_1114m_2008-2020.csv', row.names = F)




####################################################################################################

# # Abisko (SE): SA2, 1042 m # # 

# AWS: Abisko, 388 m; Abisko Aut, 392 m (temp data 2019-2020) #

####################################################################################################

# Load data
setwd(raw.dat)
tt <- read.csv('SE/temp_abisko.csv', stringsAsFactors = F) 
tt2 <- read.csv('SE/temp_abisko-aut.csv', stringsAsFactors = F) 
pp <- read.csv('SE/precip_abisko.csv', stringsAsFactors = F)


# Split date into year, month, day columns
date <- str_split_fixed(tt$date, "/", 3)
tt$year <- as.numeric(date[,3])
tt$month <- as.numeric(date[,1])
tt$day <- as.numeric(date[,2])
tt$date <- NULL

date <- str_split_fixed(tt2$date, "/", 3)
tt2$year <- as.numeric(date[,3])
tt2$month <- as.numeric(date[,1])
tt2$day <- as.numeric(date[,2])
tt2$date <- NULL

date <- str_split_fixed(pp$date, "/", 3)
pp$year <- as.numeric(date[,3])
pp$month <- as.numeric(date[,1])
pp$day <- as.numeric(date[,2])
pp$date <- NULL


# Adjust temp with MALR
delta <- (392 - 1042) * malr #(elev AWS - elev pop) * malr
tt$temp <- tt$temp + delta

delta <- (388 - 1042) * malr #(elev AWS - elev pop) * malr
tt2$temp <- tt2$temp + delta

# Combine temp DFs
tt <- rbind(tt, tt2[which(tt2$year == '19' | tt2$year == '20'), ])


# New dataframe to combine data
tp <- data.frame(year = pp$year, month = pp$month, day = pp$day, 
                 temp = NA, precip = pp$precip) 

# Unique year_month_day
tp$ymd <- paste(tp$year, tp$month, tp$day, sep = '_')
tt$ymd <- paste(tt$year, tt$month, tt$day, sep = '_')

# Combine temp and precip data
tt2tp <- match(tp$ymd, tt$ymd)
tp$temp <- tt$temp[tt2tp] 

tp$ymd <- NULL

# Standardize date columns (NOTE: this gives wrong years for pre-2000s)
for (i in 1:nrow(tp)) {
  if (nchar(tp$year[i]) == 1) {
    tp$year[i] <- paste('200', tp$year[i], sep = '') #adjust single digit years
  }
  if (nchar(tp$year[i]) == 2) {
    tp$year[i] <- paste('20', tp$year[i], sep = '') #adjust double digit years
  }
}

tp$year <- as.numeric(tp$year)


# # CLIMATE AT SA2, 2012-2020 # #

# Keep only climate corresponding to sampling years + 5 years prior
yy <- c('2007|2008|2009|2010|2011|2012|2013|2014|2015|2016|2017|2018|2019|2020')
sa2 <- tp[grep(yy, tp$year),]


# Check data 
str(sa2) #data format

summary(sa2) #data summary

par(mfrow = c(2,3))
for (i in 4:ncol(sa2)) { #data distribution
  hist(sa2[,i], breaks = 100)
  boxplot(sa2[,i], horizontal = T)
  dotchart(sa2[,i], labels = '')
}

foo <- sa2[complete.cases(sa2[3:ncol(sa2)]),]
cor(foo[4:ncol(foo)]) #correlations


# Save DF
setwd(proc.dat)
save(sa2, file = 'daily_sa2.RData')
write.csv(sa2, file = 'daily_abisko_sa2_1042m_2007-2020.csv', row.names = F)




####################################################################################################

# # Jämtland (SE): SJ1, 1137 m; SJ2, 1182 m # # 

# AWS: Blåhammaren A, 1087 m (temp); Storlien-Storvallen A, 583 (precip) #

####################################################################################################

# Load data
setwd(raw.dat)
tt <- read.csv('SE/temp_storlien-storvallen-a.csv', stringsAsFactors = F) 
pp <- read.csv('SE/precip_storlien-storvallen-a.csv', stringsAsFactors = F)


# Split date into year, month, day columns
date <- str_split_fixed(tt$date, "/", 3)
tt$year <- as.numeric(date[,3])
tt$month <- as.numeric(date[,1])
tt$day <- as.numeric(date[,2])
tt$date <- NULL

date <- str_split_fixed(pp$date, "/", 3)
pp$year <- as.numeric(date[,3])
pp$month <- as.numeric(date[,1])
pp$day <- as.numeric(date[,2])
pp$date <- NULL


# Duplicate tt for later use
temps <- tt


# # CLIMATE AT SJ1, 2011-2020 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Keep all years in DF (2009-2020)

# Adjust temp with MALR
delta <- (583 - 1137) * malr #(elev AWS - elev pop) * malr
tt$temp <- tt$temp + delta

# Combine DFs
sj1 <- data.frame(year = pp$year, month = pp$month, day = pp$day,  
                  precip = pp$precip, temp = tt$temp)


# Standardize date column
for (i in 1:nrow(sj1)) {
  if (nchar(sj1$year[i]) == 1) {
    sj1$year[i] <- paste('200', sj1$year[i], sep = '') #adjust single digit years
  }
  if (nchar(sj1$year[i]) == 2) {
    sj1$year[i] <- paste('20', sj1$year[i], sep = '') #adjust double digit years
  }
}

sj1$year <- as.numeric(sj1$year)


# Check data 
str(sj1) #data format

summary(sj1) #data summary

par(mfrow = c(2,3))
for (i in 4:ncol(sj1)) { #data distribution
  hist(sj1[,i], breaks = 100)
  boxplot(sj1[,i], horizontal = T)
  dotchart(sj1[,i], labels = '')
}

foo <- sj1[complete.cases(sj1[3:ncol(sj1)]),]
cor(foo[4:ncol(foo)]) #correlations


# Save DF
setwd(proc.dat)
save(sj1, file = 'daily_sj1.RData')
write.csv(sj1, file = 'daily_jamtland_sj1_1137m_2009-2020.csv', row.names = F)


# # CLIMATE AT SJ2, 2013-2019 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Keep all years in DF (2009-2020)

# Adjust temp with MALR
delta <- (583 - 1182) * malr #(elev AWS - elev pop) * malr
temps$temp <- temps$temp + delta


# Combine DFs
sj2 <- data.frame(year = pp$year, month = pp$month, day = pp$day,  
                  precip = pp$precip, temp = temps$temp)


# Standardize date column
for (i in 1:nrow(sj2)) {
  if (nchar(sj2$year[i]) == 1) {
    sj2$year[i] <- paste('200', sj2$year[i], sep = '') #adjust single digit years
  }
  if (nchar(sj2$year[i]) == 2) {
    sj2$year[i] <- paste('20', sj2$year[i], sep = '') #adjust double digit years
  }
}

sj2$year <- as.numeric(sj2$year)


# Check data 
str(sj2) #data format

summary(sj2) #data summary

par(mfrow = c(2,3))
for (i in 4:ncol(sj2)) { #data distribution
  hist(sj2[,i], breaks = 100)
  boxplot(sj2[,i], horizontal = T)
  dotchart(sj2[,i], labels = '')
}

foo <- sj2[complete.cases(sj2[4:ncol(sj2)]),]
cor(foo[4:ncol(foo)]) #correlations


# Save DF
setwd(proc.dat)
save(sj2, file = 'daily_sj2.RData')
write.csv(sj2, file = 'daily_jamtland_sj2_1182m_2009-2020.csv', row.names = F)




